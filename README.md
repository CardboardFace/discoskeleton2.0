# DiscoSkeleton 2.0
I created this for house parties, to overlay people's dancing over VJ software.

Utilises the Xbox Kinect 2 to draw Skeletons and effects on a transparent resizable window.

This code is not very clean, it was rushed together for entertainment purposes.

## Features
* Double click to enable transparency mode
* Scroll mouse to resize window
* Skeletons change through random bright (HSL) colours
* Raise your hands over your head and close/open your fists to change between green-screen and skeleton mode
* Touch your hands on your head to strobe your skeleton, as if being electrocuted!

## Background
Built off the BodyBasics and BodyColors samples by Microsoft